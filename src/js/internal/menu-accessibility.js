var keyCodeMap = {
  48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
  65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
  77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
  96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9"
}

$.fn.accessibility_menu = function(options) {

  //Defaults defined here. If not distinguished in function call, use these
  var settings = $.extend({
    menuClass: "menu-item-open",//The class that gets added to <li> to open menu
    mainMenuLabel: "Main Menu",//Aria-label on <nav>
    mainMenuRole: 'navigation',//Aria-role on <nav>
    topMenuRole: "menubar",//Aria-role on <ul class="fsNavLevel1">
    listItemsRole: 'menuitem',//Aria-role on <li>
    subNavRole: 'menu',//Aria-role on <ul class="fsNavLevel[1n +2]">
    firstTab: 'level2'//Tab to level 2 menu or page desc. Options: "level2" or "pagedesc"
  }, options);

  var _self = $(this);
  var dropdownWrapper = '.fsNavPageInfo';
  var fsNavLevel1 = '.fsNavLevel1';
  var subLevels = 'ul[class^="fsNavLevel"]';
  var pageDesc = '.fsNavPageDescription';
  var top_level_links = _self.find('> li > a');

  // Add ARIA attributes to the apporpriate elements based on settings
  $(this).parent()
    .attr('role', settings.mainMenuRole)
    .attr('aria-label',settings.mainMenuLabel);
  $(this)
    .attr('role', settings.topMenuRole)
    .find('li')
    .attr('role', settings.listItemsRole);
  $(this)
    .find(subLevels)
    .attr('role', settings.subNavRole);

  //Set all links in the dropdown to -1 tabindex to not worry
  $(this)
    .find(dropdownWrapper)
    .find('a').attr('tabIndex',-1);
  
  // Adding aria roles for the dropdowns / level1 <li>
  $(top_level_links).each(function(){
    if($(this).next(dropdownWrapper).length > 0) {
      $(this).parent('li')
        .attr('aria-haspopup', 'true')
        .find(dropdownWrapper)
        .attr('aria-hidden','true');
    }
  });
  
  $(top_level_links).bind("focus mouseenter mouseleave",function(){
    var allAnchors = new Array();
    $(this)
      .parents(fsNavLevel1)
      .find('> li > a').removeAttr('tabindex');
    $(this)
      .parents(fsNavLevel1)
      .find('.'+settings.menuClass)
      .removeClass(settings.menuClass)
      .find(dropdownWrapper) 
      .attr('aria-hidden', 'true')
      .find('a').attr('tabindex',-1);
    $(this)
      .next(dropdownWrapper)
      .attr('aria-hidden', 'false')
      .parent("li")
      .addClass(settings.menuClass);

    //Start populating anchor array
    allAnchors.push($(this)[0]);
    if(settings.firstTab  == 'level2') {
      if($(this).next(dropdownWrapper).find(subLevels).find("a").length) {
        for(var i=0;i<$(this).next(dropdownWrapper).find(subLevels).find("a").length;i++) {
          allAnchors.push(
            $(this)
              .next(dropdownWrapper)
              .find(subLevels)
              .find("a")[i]
          );
        }
      }
      if($(this).next(dropdownWrapper).find(pageDesc).find("a").length) {
        for(var j=0;j<$(this).next(dropdownWrapper).find(pageDesc).find("a").length;j++) {
          allAnchors.push(
            $(this)
              .next(dropdownWrapper)
              .find(pageDesc)
              .find("a")[j]
          );
        }
      }
    } else if(settings.firstTab == 'pagedesc') {
      if($(this).next(dropdownWrapper).find(pageDesc).find("a").length) {
        for(var k=0;k<$(this).next(dropdownWrapper).find(pageDesc).find("a").length;k++) {
          allAnchors.push(
            $(this)
              .next(dropdownWrapper)
              .find(pageDesc)
              .find("a")[k]
          );
        }
      }
      if($(this).next(dropdownWrapper).find(subLevels).find("a").length) {
        for(var l=0;l<$(this).next(dropdownWrapper).find(subLevels).find("a").length;l++) {
          allAnchors.push(
            $(this)
              .next(dropdownWrapper)
              .find(subLevels)
              .find("a")[l]
          );
        }
      }
    }
    for(var m=0;m<allAnchors.length;m++) {
      allAnchors[m].setAttribute("tabindex",m);
    }
  });

  $(this).on("mouseleave",function() {
    $(this)
      .find('> li > a').removeAttr('tabindex');
    $(this)
      .find("."+settings.menuClass)
      .removeClass(settings.menuClass)
      .find(dropdownWrapper) 
      .attr('aria-hidden', 'true')
      .find('a').attr('tabIndex',-1);
  });

  // Bind arrow keys for top level navigation
  $(top_level_links).keydown(function(e){
    var totalLinks = $(this).parent('li').find(dropdownWrapper).find('a').length;
    if(e.keyCode == 38) {
      //Up Arrow Key
      e.preventDefault();
      if($(this).parent('li').find(dropdownWrapper).find('a').length) {
        $(this).parent('li')
          .find(dropdownWrapper)
          .find('a[tabindex='+totalLinks+']').focus();
      }
    } else if(e.keyCode == 39) {
      //Right Arrow Key
      e.preventDefault();
      if($(this).parent('li').next('li').length == 0) {
        $(this).parents(fsNavLevel1).find('> li').first().find('a').first().focus();
      } else {
        $(this).parent('li').next('li').find('a').first().focus();
      }
    } else if(e.keyCode == 40) {
      //Down Arrow Key
      if($(this).parent('li').find(dropdownWrapper).find('a').length) {
        e.preventDefault();
        $(this).parent('li')
          .addClass(settings.menuClass)
          .find(dropdownWrapper)
          .attr('aria-hidden', 'false');
        $(this)
          .parent('li')
          .find('a[tabindex=1]').focus();
      }
    } else if(e.keyCode == 37) {
      //Left Arrow Key
      e.preventDefault();
      if($(this).parent('li').prev('li').length == 0) {
        $(this).parents(fsNavLevel1).find('> li').last().find('a').first().focus();
      } else {
        $(this).parent('li').prev('li').find('a').first().focus();
      }
    } else if(e.keyCode == 9) {
      //Tab Key
      if(e.shiftKey) {
        if($(this).parent('li').prev('li').length == 0) {
          $(this)
              .parents(fsNavLevel1)
              .find('> li > a').removeAttr('tabindex');
            $('.'+settings.menuClass)
              .removeClass(settings.menuClass)
              .find(dropdownWrapper)
              .attr('aria-hidden', 'true')
              .find('a').attr('tabIndex',-1);
        } else {
          if($(this).parent('li').prev('li').length) {
            e.preventDefault();
            var prevLinkLength = $(this).parent('li').prev('li').find(dropdownWrapper).find('a').length;
            $(this)
              .parents(fsNavLevel1)
              .find('> li > a').removeAttr('tabindex');
            $('.'+settings.menuClass)
              .removeClass(settings.menuClass)
              .find(dropdownWrapper)
              .attr('aria-hidden', 'true')
              .find('a').attr('tabIndex',-1);
            $(this)
              .parent('li')
              .prev('li')
              .addClass(settings.menuClass)
              .find(dropdownWrapper)
              .attr('aria-hidden', 'false');
            $(this).parent('li').prev('li').find(">a").focus()
              .parent().find(dropdownWrapper).find('a[tabindex='+prevLinkLength+']').focus();
          } else {
            $(this)
              .parents(fsNavLevel1)
              .find('> li > a').removeAttr('tabindex');
            $('.'+settings.menuClass)
              .removeClass(settings.menuClass)
              .find(dropdownWrapper)
              .attr('aria-hidden', 'true')
              .find('a').attr('tabIndex',-1);
          }
        }
      } else {
        if($(this).parent('li').find(dropdownWrapper).find('a').length) {
          e.preventDefault();
          $(this).parent('li')
            .addClass(settings.menuClass)
            .find(dropdownWrapper)
            .attr('aria-hidden', 'false');
          $(this)
            .parent('li')
            .find('a[tabindex=1]').focus();
        }
      }
    } else if(e.keyCode == 32) {
      //Space Bar Key
      e.preventDefault();
      window.location = $(this).attr('href');
    } else if(e.keyCode == 27) {
      //Escape Key
      e.preventDefault();
      $('.'+settings.menuClass)
        .removeClass(settings.menuClass)
        .find('> a')
        .removeAttr('tabindex')
        .parent('li')
        .find(dropdownWrapper)
        .attr('aria-hidden', 'true')
        .find('a')
        .attr('tabIndex',-1);
    } else {
      //Any other key not listed
      $(this).parent('li').find(dropdownWrapper+'[aria-hidden=false] a').each(function(){
        if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
          $(this).focus();
          return false;
        }
      });
    }
  });
  
  var subNavLinks = $(this).find(dropdownWrapper).find('a');

  $(subNavLinks).bind("focus mouseenter mouseleave",function(){
    $(this)
      .parent().parent()
      .find('.'+settings.menuClass)
      .removeClass(settings.menuClass)
      .find(dropdownWrapper)
      .attr('aria-hidden', 'true');
    $(this)
      .next(dropdownWrapper)
      .attr('aria-hidden', 'false')
      .parentsUntil(fsNavLevel1, "li")
      .addClass(settings.menuClass);
  });
  
  $(subNavLinks).keydown(function(e){
    var totalLinks = $(this).parents(dropdownWrapper).find('a').length;
    var currentIndex =  parseInt($(this).attr("tabindex"));
    if(e.keyCode == 38) {
      //Up Arrow Key
      e.preventDefault();
      if(currentIndex == 1) {
        $(this).parents(dropdownWrapper).parent('li').find('a').first().focus();
      } else {
        $(this).parents(dropdownWrapper).find('a[tabindex=' + (currentIndex - 1) + ']')
          .parentsUntil(fsNavLevel1, "li")
          .addClass(settings.menuClass)
          .find('a[tabindex=' + (currentIndex - 1) + ']').focus();
      }
    } else if(e.keyCode == 39) {
      //Right Arrow Key
      e.preventDefault();
      if($(this).parents(fsNavLevel1).find("a[tabindex='0']").parent('li').next('li').length == 0) {
        $(this).parents(fsNavLevel1).find('> li').first().find('a').first().focus();
      } else {
        $(this).parents(fsNavLevel1).find("a[tabindex='0']").parent('li').next('li').find('a').first().focus();
      }
    } else if(e.keyCode == 40) {
      //Down Arrow Key
      e.preventDefault();
      if(currentIndex == totalLinks) {
        $(this).parents(dropdownWrapper).parent('li').find('a').first().focus();
      } else {
        $(this).parents(dropdownWrapper).find('a[tabindex=' + (currentIndex + 1) + ']').focus();
      }
    } else if(e.keyCode == 27 || e.keyCode == 37) {
      //Left Arrow Key or Escape Key
      e.preventDefault();
      $('.'+settings.menuClass)
        .removeClass(settings.menuClass)
        .find(dropdownWrapper)
        .attr('aria-hidden', 'true');
      $(this)
        .parentsUntil(fsNavLevel1, "li")
        .find('a').first().focus();
    } else if(e.keyCode == 9) {
      //Tab Arrow Key
      if(e.shiftKey) {
        //Tab + Shift
        e.preventDefault();
        if(currentIndex == 1) {
          $(this).parents(dropdownWrapper).parent('li').find('a').first().focus();
        } else {
          $(this).parents(dropdownWrapper).find('a[tabindex=' + (currentIndex - 1) + ']')
            .parentsUntil(fsNavLevel1, "li")
            .addClass(settings.menuClass)
            .find('a[tabindex=' + (currentIndex - 1) + ']').focus();
        }
      } else {
        if(currentIndex == totalLinks) {
          if($(this).parents(dropdownWrapper).parent('li').next('li').length) {
            e.preventDefault();
            $(this).parents(dropdownWrapper).parent('li').next('li').find('a').first().focus();
          } else {
            $(this)
              .parents(fsNavLevel1)
              .find('> li > a').removeAttr('tabindex');
            $('.'+settings.menuClass)
              .removeClass(settings.menuClass)
              .find(dropdownWrapper)
              .attr('aria-hidden', 'true')
              .find('a').attr('tabIndex',-1);
          }
        } else {
          e.preventDefault();
          $(this).parents(dropdownWrapper).find('a[tabindex=' + (currentIndex + 1) + ']').focus();
        }
      }
    } else if(e.keyCode == 32) {
      e.preventDefault();
      window.location = $(this).attr('href');
    } else {
      var found = false;
      $(this).parent('li').nextAll('li').find('a').each(function(){
        if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
          $(this).focus();
          found = true;
          return false;
        }
      });
      if(!found) {
        $(this).parent('li').prevAll('li').find('a').each(function(){
          if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
            $(this).focus();
            return false;
          }
        });
      }
    }
  });

  $(document).click(function(){
    $(this)
      .parents(fsNavLevel1)
      .find('> li > a').removeAttr('tabindex');
    $('.'+settings.menuClass)
      .removeClass(settings.menuClass)
      .find(dropdownWrapper)
      .attr('aria-hidden', 'true')
      .find('a').attr('tabIndex',-1);
  });
  
  $(this).click(function(e){
    e.stopPropagation();
  });
}
