const ev = require('./export-vars.js');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const browserSync = require('browser-sync').create();
const compiler = ev.wp(ev.wpDevConfig);

//start here working (css compilation && npm run local)

// browserSync.watch("./src/sass/**/*.scss", function (event, file) {
browserSync.watch("./src/**/**/*.scss", function (event, file) {
    if (event === "change") {
        browserSync.reload("./dist/main.css");
    }
});


browserSync.init({
    host: 'localhost',
    port: 3000,

    proxy: {
      target: ev.pkg.config.siteurl + '/' + ev.pkg.config.startpage,
      ws: true,

      middleware: [

        webpackDevMiddleware(compiler, {
          // IMPORTANT: dev middleware can't access config, so we should
          // provide publicPath by ourselves
          publicPath: ev.wpDevConfig.output.publicPath,

          // pretty colored output
          stats: { colors: true }
        }),

        // bundler should be the same as above
        webpackHotMiddleware(compiler, {
          log: console.log, 
          path: '/__webpack_hmr', 
          heartbeat: 10 * 1000
        }),
      ]
    },
    notify: false,
});
