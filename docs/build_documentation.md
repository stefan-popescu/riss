fs-webpack-build
==========================

https://webpack.js.org/

- package.json "config" replacing site-vars.js <br />
- fonts and images will not render/compile unless specifically used in your sass files. Any images not referenced or specifically used in your build will not be uploaded to the server. You should add any images that aren't being specified in your code through composer (ie resources or file manager) and not by adding them to the images folder.

<br />

## project workflow:
- MACS make sure you have nvm (node version manager) installed 
- check node version ($ node -v) and make sure it is above v8.6.0 or the lastest LTS version
- check npm version ($ npm -v) and make sure it is above v4.6.0 or the lastest permissable version (this is where a versioning manager is very helpful)

- delete old node_modules and reinstall 'npm install'
- update package.json 'config' parameters to match project site information
- PC - add letter drive you wish to map to in config area
- if you are working on a district site, add your list of schools in district-vars.js

- $ npm run publish 
  > MAC - this will connect you first to the server if you are not connected<br />
  > PC - this will check if your drive is mapped, if it is this step will delete your mapped drive<br />
  > run 'npm run publish' again to publish files to live server and map your drive<br />


- Once you have published your project for the first time, you can work locally by using $ npm start

<br />

**see README.md for further instructions - https://stash.fs4.us/projects/DPL/repos/fs-webpack-build/browse/README.md**

<br />

## JS breakdown: 
- you no longer will need to move any plugins into any other folders to use them

**src/js/...**<br />

- /include - all of your site specific javascript is written in these files
- /internal - finalsite specific javascript plugins
- /vendor - common vendor plugins used from the interweb<br />


**src/js/include/... files define categorized objects & method properties**<br />

- accessibility - accessibility specific js 
- config-vars - defines ES6 constants (also known as "immutable variables"), i.e., variables which cannot be re-assigned new content.
- default-styles - js relating to any styles manager class or necessary, basic js for your site that wouldn't be considered an enhancement (i.e. hero images)
- enhancements - enhancement specific javascript (i.e. image buttons, infographics, custom extra functionality not included in a basic package)
- home - homepage only related javascript
- index - runs all the above object methods, very helpful for debugging! 
- navigation - include off canvas, collapsable sub navs etc
- responsive-slideshow-subtype - element slideshow subtypes responsive corrective rendering


**these files should cover the majority of javascript categories we use, but you can always add more files here just make sure you include them in your index.js file**

- build.js is where you require your main.scss, global.js and any other component/BPA index.js files

<br />

## SASS breakdown:

https://stash.fs4.us/projects/DPL/repos/fs-webpack-build/browse/src/sass/include.scss


> config sass files and utility sass features are separate from main.scss and processed through webpack so that when you clone components (ie BPA repositories) into your build they are automatically available 

> if you need global access to variables or mixins etc, they will need to be added to include.scss

<br />

## All Terminal Commands: 

```
$ npm run admin = opens the sitename.finalsite.com/admin/fs login page (mac only)
$ npm run adminPC = opens the sitename.finalsite.com/admin/fs login page (pc only)
$ npm run server = connects to server (mac only)
$ npm run district-setup = creates sass & js files from district-vars.js object (mac & pc)
$ npm run district-production": runs district specific webpack production environment for building on the server (mac & pc)
$ npm run production = runs webpack production environment for building on the server (mac & pc)
$ npm run live = checks connection to server, if not connected runs server command then run again to build on live server (mac only)
$ npm run local = runs webpack development environment for building locally (mac & pc)
$ npm run localPC = opens login page & local dev environment (pc only)
$ npm run map-remove = checks if drive is mapped, if one exists it will delete the mapped drive (pc only)
$ npm run map-create = creates mapped drive (pc only)
$ npm run map = if mapped drive exists 'npm run map-remove' else 'npm run map-create' && 'npm run production' (pc only)
$ npm run map-district = if mapped drive exists 'npm run map-remove' else 'npm run map-create' && 'npm run district-setup' && 'npm run district-production' (pc only)
$ npm run districtMAC = checks connection to server, then 'npm run district-production', else 'npm run server' (mac only)
$ npm run districtPC = 'npm run map-district' (pc only)
$ npm run publishMAC = checks if district is set to true, if true 'npm run district-setup && npm run districtMAC', else 'npm run live' (mac only)
$ npm run publishPC = checks if district is set to true, if true 'npm run districtPC' else 'npm run map' (pc only)

$ npm start = checks operating system, opens login page & runs appropriate local development environment (mac & pc)
$ npm start:win32 = 'npm run localPC'
$ npm start:darwin:linux = 'npm run admin && node server.js'

$ npm run publish = checks operations system & runs appropriate production environment (mac & pc)
$ npm run publish:win32 = 'npm run publishPC'
$ npm run publish:darwin:linux = 'npm run publishMAC'
```

<br />

## Mac Terminal Commands:

```
$ npm run admin = opens the sitename.finalsite.com/admin/fs login page
$ npm run server = connects to server
$ npm run district-setup = creates sass & js files from district-vars.js object (mac & pc)
$ npm run district-production": runs district specific webpack production environment for building on the server (mac & pc)
$ npm run production = runs webpack production environment for building on the server (mac & pc)
$ npm run live = checks connection to server, if not connected runs server command then run again to build on live server
$ npm run local = runs webpack development environment for building locally (mac & pc)
$ npm run districtMAC = checks connection to server, then 'npm run district-production', else 'npm run server'
$ npm run publishMAC = checks if district is set to true, if true 'npm run district-setup && npm run districtMAC', else 'npm run live'
$ npm start = opens login page & local dev environment
$ npm start = checks operating system, opens login page and runs appropriate local development environment (mac & pc)
$ npm start:darwin:linux = 'npm run admin && node server.js'
$ npm run publish = checks operations system and runs appropriate production environment (mac & pc)
$ npm run publish:darwin:linux = 'npm run publishMAC'
```

<br />

## PC Terminal Commands: 

```
$ npm run adminPC = opens the sitename.finalsite.com/admin/fs login page
$ npm run district-setup = creates sass & js files from district-vars.js object (mac & pc)
$ npm run district-production": runs district specific webpack production environment for building on the server (mac & pc)
$ npm run production = runs webpack production environment for building on the server (mac & pc)
$ npm run local = runs webpack development environment for building locally (mac & pc)
$ npm run localPC = opens login page & local dev environment
$ npm run map-remove = checks if drive is mapped, if one exists it will delete the mapped drive
$ npm run map-create = creates mapped drive
$ npm run map = if mapped drive exists 'npm run map-remove' else 'npm run map-create' && 'npm run production'
$ npm run map-district = if mapped drive exists 'npm run map-remove' else 'npm run map-create' && 'npm run district-setup' && 'npm run district-production' (pc only)
$ npm run districtPC = 'npm run map-district'
$ npm run publishPC = checks if district is set to true, if true 'npm run districtPC' else 'npm run map'
$ npm start = checks operating system, opens login page & runs appropriate local development environment (mac & pc)
$ npm start:win32 = 'npm run localPC'
$ npm run publish = checks operations system & runs appropriate production environment (mac & pc)
$ npm run publish:win32 = 'npm run publishPC'
```

<br />
local/development environment = localhost:3000 <br />
live/production environment = localhost:5000 <br />







