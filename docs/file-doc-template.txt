## SCSS file header ##

1. Describe file and where it lives in the directory tree
		/*! _twitterCustom.scss - src/sass/modules/social/ - social twitter custom */

2. Describe what the partial does
		// this custom element allows you pull the feed out of the iframe to add custom styles

3. List links to example pages on http://coderepo.demo.finalsite.com
		// example page: http://coderepo.demo.finalsite.com/production/social-elements/custom-social-feeds and
		// example page http://coderepo.demo.finalsite.com/production/social-elements/twitter-element

4. List any javascript requirements
		// must move twitterCustom.js from src/js/optional to src/js/plugins and call function in global.js or enhancements.js

5. Describe how to implement the partial in Composer
		// add 'twitterCustom' class to twitter element on page



## JS file header ##

1. Describe file and where it lives in the directory tree
		/*! twitterCustom.js - social twitter custom - src/js/optional */

2. Describe what the function does
		//Retrieve and process twitter feeds for custom styling

3. List links to example pages on http://coderepo.demo.finalsite.com
		//example site: http://coderepo.demo.finalsite.com/production/social-elements/custom-social-feeds

4. Include any scss requirements
		//sass: modules > social > _twitterCustom.scss

5. Describe how to use function and give example
		//twitterCustom('.twitterCustom'); //target specfic element
		//twitterCustom('.fsTwitter') //make all twitter elements custom


## Page Header ##

1. Name of element (descriptor)
		//Twitter Custom Element

2. List scss and js requirements:
		//src/js/optional/customTwitter.js 
		//src/sass/modules/social/_twitterCustom.scss

3. Include instructions on how to implement element
		//Javascript plugin must be moved into the plugins folder. Call the function in your main javascript file for the plugin to run.

4. Include any code examples
		//example with custom class on twitter element: twitterCustom('.twitterCustom');
		//example to run on all twitter elements: twitterCustom('.fsTwitter');

5. Include any other instructions
		//Twitter plugin may require tweaking depending on your design.


