exports.wp = require('webpack');
exports.ExtractTextPlugin = require("extract-text-webpack-plugin");
exports.CleanWebpackPlugin = require('clean-webpack-plugin');
exports.GitRevisionPlugin = require('git-revision-webpack-plugin');
exports.UglifyJsPlugin = require('uglifyjs-webpack-plugin');


exports.pkg = require('./package.json');


var os = process.platform;
var sitepath, themepath, checkThemePath, execType;
var pkgJSON = require('./package.json');

if( os === 'darwin' ){
  exports.directory = '/Volumes/' + pkgJSON.config.sitename + '/uploaded/themes/' + pkgJSON.config.themepath + '/';
}else if( os === 'win32' ){
  //should default to Windows
  exports.directory =  pkgJSON.config.pcdrive + '\\' + pkgJSON.config.sitename + '/uploaded/themes/' + pkgJSON.config.themepath + '/';
  // exports.directory =  pkgJSON.config.pcdrive + '\\demo/' + pkgJSON.config.sitename + '/uploaded/themes/' + pkgJSON.config.themepath + '/';
}else{
  exports.directory = null;
  console.log('Add os type to script');
}

//districts


if (pkgJSON.config.district) {
  exports.wpDevConfig = require('./district/district-wp.development.js');
  exports.wpProdConfig = require('./district/district-wp.production.js');
} else {
  exports.wpDevConfig = require('./webpack/webpack.development.js');
  exports.wpProdConfig = require('./webpack/webpack.production.js');
}
