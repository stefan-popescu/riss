// // ====================================================
// // Custom Media Element Image Slider - ADA Compliant
// // ====================================================

// this function is used in conjunction with the media element and any element which you want to make into a slider
// For Media Element it only applies when the 'Use custom player' setting is checked,
    // the option for the custom player is located under 'Advanced Settings' inside of the 'Media Element Settings'
    // the styling is located in the modules/media sass partial
    // example: http://coderepo.demo.finalsite.com/stuff-by-al/ada-slideshow

// // use case:
// $('.universal-slideshow').each(function() {
//   var self = $(this);
  
//   self.mediaSlider({
//     mediaTemplate:[
//       '<article class="universal-slide" style="background-image: url({imgSrc});">',
//         '<img src="{imgSrc}" alt="{captionTitle}" class="universal-img" />',  // keep an <img> with an alt attribute in the slide
//         '<div class="caption-wrapper">',
//           '<div class="caption-title">{captionTitle}</div>',
//           '<div class="caption-desc">{captionDesc}</div>',
//         '</div>',
//       '</article>'
//     ], // html markup
//     slick: {
//         // define your custom slick settings here
//     },
//     bp: 600,   //when to pull in mobile size images
//     preSlickCallback: function(element) {}, //code to run before slick inits
//     callback: function(element) {

//         //       write all functions to be run after media data has been pulled in here
//         //       ie (randomize, etc...)
//         //       defaults to null

//     }
//   });
// });


//get element
//check if media element or element with children

require('./fs-media-pull');

function Slider(element, options) {
  var _ = this,
  slickCall
  ;

  _.element = element,
  _.isMedia = false,
  _.autoplay = false,
  _.randomize = false,
  _.html = ""
  ;

  if(_.element.classList.contains("fsMedia")) {
    _.isMedia = true;
  }
  else if(_.element.classList.contains("fsMediaCustomPlayer")) {
    _.element = $(_.element).parents(".fsMedia")[0];
    _.isMedia = true;
  }

  if(_.isMedia) {
    _.autoplay = $.parseJSON($(_.element).find(".fsMediaCustomPlayer").attr("data-autoplay"));
    _.randomize = $.parseJSON($(_.element).find(".fsMediaCustomPlayer").attr("data-randomstart"));
    _.url = $(_.element).find(".fsMediaCustomPlayer").attr("data-playlisturl");
  }

  slickCall = {
    slidesToShow: 1,
    accessibility: true,
    dots: true,
    arrows: true,
    autoplay: _.autoplay,
    pauseOnHover: false,
    adaptiveHeight: true,
    initialSlide: _.randomize ? randomizeSlideshow() : 0
  };
  
  function randomizeSlideshow() {

    $.getJSON(_.url).done(function (data) {
      slickCall["initialSlide"] = parseInt(Math.ceil(Math.random() * data.objects.length));
      startInit();
    });

  };

  _.defaults = {
    mediaTemplate: [
      '<article class="universal-slide">',
        '<img src="{imgSrc}" alt="{captionTitle}" class="universal-img" />',
        '<div class="caption-wrapper">',
          '<div class="caption-title">{captionTitle}</div>',
          '<div class="caption-desc">{captionDesc}</div>',
        '</div>',
      '</article>'
    ],
    slick: slickCall,
    bp: 600,
    preSlickCallback: null,
    callback: null
  }

  function startInit() {
    _.settings = $.extend(true, {}, _.defaults, options);
    _.init();
  };

  if(!_.randomize) {
    startInit();
  }
};

Slider.prototype = {
  init: function () {
    var _ = this;

    //slider parent (what we'll call slick on)
    _.slider = (_.isMedia) ? _.element.getElementsByClassName("fsMediaCustomPlayer")[0] : _.element;

    //add class to slider for styling (/modules/_media.scss)
    _.slider.classList.add("fsCustomSlider");

    //join mediaTemplate into string if Array
    _.html = (Array.isArray(_.settings.mediaTemplate)) ? _.settings.mediaTemplate.join("\n") : _.settings.mediaTemplate;

    if(_.isMedia) {
      _.sliderPrep();
    }
    else {
      if(!document.body.classList.contains("fsDraftMode")) {
        //init slick when not in compose mode
        _.slickInit();
      }
    }
  },

  sliderPrep: function () {
    var _ = this;

    //get slides from Media Manager
    $(_.element).mediaPull({
      mediaTemplate: _.settings.mediaTemplate,
      bp: _.settings.bp,
      callback: function () {
        var checkSlide = setInterval(function(){
            if( $(_.element).find('img').length > 0 ){
                clearInterval(checkSlide);
                _.slickInit();
            }
        },100);
      }
    });
  },

  slickInit: function () {
    var _ = this;

    var $slider = $(_.slider);

    $slider.on("init", function (ev, slick) {

      if(_.settings.slick.autoplay && slick.$slides.length > 1) {

        //add play/pause button only if autoplay is true
        var playBtn = str2DOM("<button class='slider-play-btn'>Play</button>");

        playBtn.addEventListener("click", function () {
          slickPlayPause(_.slider);
        });

        _.slider.insertBefore(playBtn, _.slider.firstChild);


        //check if autoplay is on, if it is add playing class to slider
        if(slick.options.autoplay) {
          _.slider.classList.add("slider-playing");
        }
        else {
          _.slider.classList.add("slider-paused");
        }

      }

      //callback function
      if(typeof _.settings.callback == "function") {
        _.settings.callback.call(_, _.element);
      }
    });

    //pre slick callback
    if(typeof _.settings.preSlickCallback == "function") {
      _.settings.preSlickCallback.call(_, _.element);
    }

    $slider.slick(_.settings.slick);
  }
};

//convert string to DOM element
function str2DOM (html) {
  var el = document.createElement('div');
  el.innerHTML = html;
  return el.childNodes[0];
}

function slickPlayPause (slider) {

  //toggle play/pause class
  slider.classList.toggle("slider-playing");
  slider.classList.toggle("slider-paused");

  if(slider.classList.contains("slider-playing")) {
    $(slider).slick("slickPlay");
  }
  else {
    $(slider).slick("slickPause");
  }
}

$.fn.mediaSlider = function(options) {
  this.each(function () {
    new Slider(this, options);
  });
}

