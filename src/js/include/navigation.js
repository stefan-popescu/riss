// ================================
// NAVIGATION
// ================================

require('../vendor/doubletaptogo');

import {
  $body,
  $navSub,
  $navSub_title,
  mobileMenuToggle,
  sectionTitle
} from './config-vars';

NAVIGATION = {

  init () {
    this.offCanvasMenu();
    this.collapseSubNav();
    this.sectionTitleSubNav();
  }, 
  

  // Off Canvas Menu
  // ==================================================

  offCanvasMenu () {
      // Toggle attribute of the body
      $(mobileMenuToggle).on('click', function() {
          $body.toggleClass('drawer-is-active');
      });

      // Remove attribute on the bottom if anything other than
      // what is mentioned is clicked on
      $(document).on('click', function(event) {
          if (!$(event.target).closest('#fsMenu, .mobile-toggle').length) {
              $body.removeClass('drawer-is-active');
          }
      });
  },


  // collapsable subnav on mobile
  // ==================================================

  collapseSubNav () {
    // nav-sub - mobile toggle
    $navSub_title.on('click', function() {
        $(this).closest($navSub).toggleClass('active-nav');
    });

    // nav-sub remove click elsewhere
    $(document).on('click', function(event) {
        if (!$(event.target).closest($navSub).length) {
            $navSub.removeClass('active-nav');
        }
    });    
  },


  // Create a section title based on the current page
  // ==================================================

  sectionTitleSubNav () {
      if (sectionTitle.length !== 0) {
          $navSub_title.html(sectionTitle);
      }

      if ($navSub.find('nav .fsNavLevel1').length !== 0) {
          $navSub.removeClass('nav-sub-empty');
      } else {
          $navSub.addClass('nav-sub-empty');
      }

  },

}; //end NAVIGATION

export default NAVIGATION;