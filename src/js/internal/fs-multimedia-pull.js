/* Usage
 1. Create a container element and give it a class to reference
 2. Add multiple media elements to the container
		- add elements in the order they should appear in the slideshow
		- check the "use custom player option"
		- use video or photo slideshows
 3. Add the following function to global.js using the classname you applied to the container
		- $('.multimedia-slideshow').fsMultimediaSlideshow();

 */

//functions
// getMedia
// buildPhotos

$.fn.fsMultimediaSlideshow = function( options ) {

	//define default settings
	var defaults = {
		bp : 600
	};


	function getMedia(el, callback) {

		var media = el.find('.fsMediaCustomPlayer');
		var jsonURL = media.attr('data-playlisturl');

		media.data('display_loaded', false);

		$.getJSON( jsonURL )

			.done(function(data) {
if (callback && typeof(callback) === "function") {
					callback(data, el, media);
				}

			})

		.fail(function() {

			media.append('<span>Please make sure you have content added to media manager and that you have selected the correct element settings.</span>').css('textAlign', 'center');

		});

	} //getMedia




	function buildPhotos(data, el, media, template) {

		var bp = 600;
		var win = $(window);
		var media_path, bgPath;

		if (win.width() > bp) {
			media_path = "full";
		} else {
			media_path = "mobile";

			win.on('resize', function() {
				if (win.width() > bp && !media.data('display_loaded')) {
					win.data('display_loaded', true);

					// media.find('article').each(function() {
					$('.fsMediaCustomPlayer').find('article').each(function() {
						var _ = $(this);

						var changeSrc = _.find("img").attr('src').replace('/mobile/', '/fullsize/');

						_.find("img").attr('src', changeSrc);
						_.css('background-image', 'url("' + changeSrc + '")');
						_.children('.multimedia-img-wrapper').css('background-image', 'url("' + changeSrc + '")');
					});

				}

				});
			}

			var set = $();

			$.each( data.objects, function( i, object ) {
				if ( media_path === "full" ) {
					bgPath = data.objects[i].full_path;
				} else {
					bgPath = data.objects[i].mobile_path;
				}

				var slide = nano(template.slide, {
					'imgSrc': bgPath,
					'captionTitle': data.objects[i].object_title,
					'captionDesc': data.objects[i].object_description,
				});

				set = set.add(slide);
			});

			return set;

		} //buildPhotos



		// Video and Image Main Slideshow
		$(this).each(function(i) {

			this.settings = $.extend( {}, defaults, options );

			var _ = $(this),
			elementContent = _.children('.fsElementContent'),
			ajaxQueue = 0,
			buildQueueComplete = false,
			win = $(window),
			bp = this.settings.bp,
			$noTouch = $('.no-touch'),
			$touch = $('.touch'),
			slideTemplate = { // slide template structure

				'slide': [
					'<article class="multimedia-slide" style="background-image: url({imgSrc});">',
					'<div class="multimedia-img-wrapper" style="background-image: url({imgSrc});"><img src="{imgSrc}" class="multimedia-img" /></div>',
					'<div class="caption-wrapper">',
					'<div class="caption-title">{captionTitle}</div>',
					'<div class="caption-desc">{captionDesc}</div>',
					'</div>',
					'</article>'
				].join('\n')

			};

			_.addClass('fsMediaCustomPlayer');
			_.append('<div class="multimedia-controls"></div>');

			elementContent.on({

				'init': function( event, slick) {
					var video = slick.$slides.eq( slick.currentSlide ).find( 'video' );

					if ( win.width() > bp) {

						if ( video.length && $noTouch.length ) {
							video[0].play();

							video.on( 'ended', function( event ) {
								slick.slickNext();
							});

						} else {
							slick.slickPlay();
						}

					} else {

						slick.slickPlay();

					}
				},

				'beforeChange': function( event, slick, currentSlide, nextSlide ) {

					var video = slick.$slides.eq( slick.currentSlide ).find( 'video' );

					if ( win.width() > bp ) {
						if ( video.length && $noTouch.length ) {
							video[0].pause();

							slick.slickPlay();

						} else {
							slick.slickPause();
						}

					} else {
						slick.slickPlay();
					}

				},

				'afterChange': function( event, slick, currentSlide ) {

					var video = slick.$slides.eq( slick.currentSlide ).find( 'video' );

					if ( win.width() > bp ) {

						if ( video.length && $noTouch.length ) {
							video[0].play();

							video.on( 'ended', function( event ) {
								slick.slickNext();
							});

						} else {

							slick.slickPlay();

						}

					} else {

						slick.slickPlay();

					}

				},

				'checkQueue': function(e) {
					if (!buildQueueComplete) {
						return;
					} else {
						if (!ajaxQueue) {
							elementContent.slick({
								'dots': false,
								'arrows': true,
								'appendArrows': _.children('.multimedia-controls'),
								'fade': false,
								'adaptiveHeight': false,
								'accessibility': false,
								'autoplay': false,
								'pauseOnHover': false,
								'speed': 1000,
								'autoplaySpeed': 8000,
								'responsive': [
								{
									'breakpoint': 600,
									'settings': {
										'dots': false,
										'arrows': true,
										'appendArrows': _.children('.multimedia-controls'),
										'fade': false,
										'adaptiveHeight': false,
										'accessibility': false,
										'autoplay': false,
										'pauseOnHover': false,
										'speed': 1000,
										'autoplaySpeed': 8000,
									}
								}
								]
							});

						}

					}

				}

			});

			elementContent.children().each(function(i) {

				var self = $(this);

				if (self.hasClass('fsMedia') && self.children('.fsElementContent').children('.fsMediaContainer').hasClass('fsMediaCustomPlayer')) {

					ajaxQueue++;

					getMedia(self, function(data, el, media) {
						var win = $(window);

						if (data.group_type === 1) { // Photo

							self.html( buildPhotos( data, el, media, slideTemplate ) );
							self.children().eq(0).unwrap();

						} else if ( data.group_type === 3 ) { // Video

							$.each( data.objects, function(i) {
								var object = data.objects[i],
								poster = object.mobile_path,
								src;


								if (win.width() > 600 && $noTouch.length ) {
									src = object.hd_video_path;
								} else if ( $touch.length ){
									src = object.mobile_video_path;
								} else {
									src = object.mobile_video_path;
								}

								var $slide = $('<article class="multimedia-slide video-slide" />'),
								$video = $('<div class="video-wrapper" style="background-image: url(' + poster + ')"><video src=' + src + ' muted /></div>'),
								$caption = $('<div class="caption-wrapper"><div class="caption-title">' + object.object_title + '</div><div class="caption-desc">' + object.object_description + '</div></div>');

								self.html($slide.append($video).append($caption));

								// self.html($slide.append($video).append($caption));
								self.children().eq(0).unwrap();
								$video[0].muted = true;
								$video[0].controls = false;
								$video[0].autoplay = false;
							});
						}

						ajaxQueue--;
						elementContent.trigger('checkQueue');
					});
				} else {
					self.remove();
				}
			});

			buildQueueComplete = true;
		});
	}
