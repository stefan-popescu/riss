// this file creates the individual school partials

var fs = require('fs');
var ev = require('../export-vars.js');
var dv = require('./district-vars.js');


DISTRICT_SETUP = {

  init () {

    this.createDistrictSassFiles();
    this.createDistrictJSFiles();

  },

  createDistrictJSFiles () {

    dv.district.schools.forEach(function(name) {
      var schoolJS = 'src/district/js/' + name + '.js';
      var schoolHeader = '// ' + name + '.js';
      var schoolRequire = '\n\nrequire("../sass/'+ name +'.scss");\nrequire("./all.js");';

      if(!fs.existsSync(schoolJS)) {
        fs.writeFileSync(schoolJS, schoolHeader + schoolRequire);
        console.log('created ' + schoolHeader)
      } else {
        console.log('school ' + schoolHeader + ' exists')
      }
    });
  },

  createDistrictSassFiles () {

    dv.district.schools.forEach(function(name) {
      var schoolSass = 'src/district/sass/' + name + '.scss';
      var schoolHeader = '// ' + name + '.scss';
      var schoolImport = '\n\n@import "all";';

      if(!fs.existsSync(schoolSass)) {
        fs.writeFileSync(schoolSass, schoolHeader + schoolImport);
        console.log('created ' + schoolHeader)
      } else {
        console.log('school ' + schoolHeader+ ' exists')
      }
    });
  }

} // end DISTRICT_SETUP

DISTRICT_SETUP.init();

