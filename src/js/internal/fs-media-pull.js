// // ====================================================
// // Pull Images from Media Manager
// // ====================================================

// this function is used in conjunction with the media element
// it only applies when the 'Use custom player' setting is checked,
// the option for the custom player is located under 'Advanced Settings' inside of the 'Media Element Settings'
// If using on a Media element call on .fsMedia otherwise if using it on another element and pulling in from a media gallery,
// call it on element you want to append to.

// // use case if calling on Media Module (will append to fsMediaCustomPlayer):
// $(".media-element").mediaPull({
//   mediatemplate: [
//     '<article class="universal-slide">',
//      '<img src="{imgSrc}" alt="{captionTitle}" class="universal-img" />',
//       '<div class="caption-wrapper">',
//         '<div class="caption-title">{captionTitle}</div>',
//         '<div class="caption-desc">{captionDesc}</div>',
//       '</div>',
//     '</article>'
//   ], //default markup

//   callback: function() {

//     // write all functions to be run after media data has been pulled in here
//     // ie (slick, randomize, etc...) 
//     // NOTE: technically you can slick here but preferably you will use fs-media-slider.js for ADA compliance

//   },
//   bp: 600, //when to switch to mobile images
//   url: "" //data-playlisturl for media manager gallery you want to pull from
// });
//
// // use case for non Media Module (will append to element it was called on)
// $(".other-element .fsElementContent").mediaPull({
//    url: ""/cf_endpoints/routes.cfm/media/groups/85/objects.json"
// });

import {
  nano
} from './utilities';

function MediaPull(element, options) {
  var _ = this;

  var defaults = {
    mediaTemplate: [
      '<article class="universal-slide">',
        '<img src="{imgSrc}" alt="{captionTitle}" class="universal-img" />',
        '<div class="caption-wrapper">',
          '<div class="caption-title">{captionTitle}</div>',
          '<div class="caption-desc">{captionDesc}</div>',
        '</div>',
      '</article>'
    ],
    bp: 600,
    callback: null,
    url: null
  };

  _.element = element;
  _.container = element; //where to append pulled content
  _.settings = $.extend(true, {}, defaults, options);
  _.url = "";

  _.init();
}

MediaPull.prototype = {
  init: function () {
    var _ = this;

    //if element is fsMedia then get url from fsMediaCustomPlayer
    if(_.element.classList.contains("fsMedia")) {
      //if fsMedia make container fsMediaCustomPlayer
      _.container =  _.element.getElementsByClassName("fsMediaCustomPlayer")[0];
      _.url = _.container.getAttribute("data-playlisturl");
    }
    else if(_.settings.url) {
      _.url = _.settings.url;
    }

    //media template (join into string if array)
    _.html = (Array.isArray(_.settings.mediaTemplate)) ? _.settings.mediaTemplate.join("\n") : _.settings.mediaTemplate;

    _.getContent();
  },

  getContent: function () {
    var _ = this;

    $.getJSON(_.url).done(function (data) {
      var obj = data.objects;

      for(var i = 0; i < obj.length; i++) {
        //create nano string with media data then convert string to DOM element
        var slide = str2DOM(nano(_.html, {
            'imgSrc': (window.innerWidth > _.settings.bp) ? obj[i].full_path : obj[i].mobile_path, //if viewport width is bigger than breakpoint pull in full_path otherwise pull in mobile
            'captionTitle': obj[i].object_title,
            'captionDesc': obj[i].object_description
        }));

        //check if caption is empty
        if(slide.textContent.trim().length == 0 && slide.getElementsByClassName("caption-wrapper").length) {
          slide.getElementsByClassName("caption-wrapper")[0].classList.add("is-empty");
        }

        //append slide to slider
        _.container.appendChild(slide);
      }

      _.callback();
    });

  },

  callback: function () {
    var _ = this;

    //callback function
    if(typeof _.settings.callback == "function") {
      _.settings.callback.call();
    }
  }
};

//convert string to DOM element
function str2DOM(html) {
  var el = document.createElement('div');
  el.innerHTML = html;
  return el.childNodes[0];
}

$.fn.mediaPull = function(options) {
  this.each(function () {
    new MediaPull(this, options);
  });
}

