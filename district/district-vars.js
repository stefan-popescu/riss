// this file is where you define your individual school variables to be used throughout the build files
const path = require('path');
const hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';

// district.js object: add schools here
exports.district = {
  schools: [
    "school1",
    "school2",
    "school3"
  ]
};

var PATHS = {
  context: path.resolve(__dirname, "../src/"),
  form: path.join(__dirname, "../src/js/form"),
  main: path.join(__dirname, "../src/build.js"),
};

exports.entryDEV = {
  main: [PATHS.main, hotMiddlewareScript],
  form: [PATHS.form, hotMiddlewareScript],
};

exports.entryPROD = {
  main: [PATHS.main],
  form: [PATHS.form],
};

exports.district.schools.forEach(function(name){
	PATHS[name] = path.join(__dirname, "../src/district/js/"+name+".js");
	exports.entryDEV[name] = [PATHS[name], hotMiddlewareScript];
	exports.entryPROD[name] = [PATHS[name]];
});
