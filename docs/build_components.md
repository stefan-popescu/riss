Components: 
==========================

```
$ cd <the folder you want it in>
$ git clone <the repo you want>
```
<br />

## EXAMPLE:
```
$ cd src/components
$ git clone https://user.name@stash.fs4.us/scm/dpl/fed-components/bpa/slideshows/fiftyfifty.git
```
<br />

## Components Folder Outline: 
src/components/ is where the BPA files live and are included in the build

## Structure:
src/components/bpa-type/bpa.js<br />
src/components/bpa-type/bpa.scss<br />
src/components/bpa-type/index.js<br />

- index.js includes required bpa.js & bpa.scss: 
```
require('./bpa.js');
require('./bpa.scss’);
```

<br />

- which is then referenced in src/build.js:

```
require('./components/bpa-type/index.js');
```