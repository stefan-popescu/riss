// =============================
// Common Utitlies
// =============================

export function backgroundImage($element) {
  backgroundElement = $element;
  $(backgroundElement).each(function() {

    var image = $(this).find('img').attr('src');

    $(this).css('background-image', 'url("' + image + '")');

  });
};

// ===================================
// Debouncing for Window Resize Events
// ===================================

export function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  }
};

// ===================================
// Search Module Placeholder
// Use: placeholder($(".search-module"), "Search");
// Note: function MUST be called after $.support.placeholder function below this comment
// ===================================

// //check if browser supports placeholders for placeholder()
// $.support.placeholder = (function() {
//     var i = document.createElement('input');
//     return 'placeholder' in i;
// })();

export function placeholder(el, text) {

    var counter = 100, // number of attempts before the function self-terminates
        interval = 100, // milliseconds between attempts
        searchCheck,
        timer
    ;

    searchCheck = function searchCheck(){
        if(el.find("input.gsc-input").length){
            if($.support.placeholder) {
                el.find("input.gsc-input").attr("placeholder", text);
            }
            else {
                el.find("input.gsc-input").attr("value", text);
            }
        } else if( counter > 0 ) {
            timer = setTimeout( searchCheck, interval );
            counter -= 1;
        }
    };

    timer = setTimeout( searchCheck, interval );
};

// =============================
// Nano Templates
// https://github.com/trix/nano
// =============================

export function nano(template, data) {
  return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
    var keys = key.split("."), v = data[keys.shift()];
    for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
    return (typeof v !== "undefined" && v !== null) ? v : "";
  });
};

// =============================
// Randomize Function
// =============================

export function randomGenerator() {
  $.fn.randomize = function(selector){
    var $elems = selector ? $(this).find(selector) : $(this).children(),
        $parents = $elems.parent();

    $parents.each(function(){
        $(this).children(selector).sort(function(){
            return Math.round(Math.random()) - 0.5;
        }).detach().appendTo(this);
    });

    return this;
  };
};


