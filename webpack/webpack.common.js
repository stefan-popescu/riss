// ==========================================================================
// Site Project Config
// ==========================================================================
"use strict";

// Required Components
// ==========================================================================
const ev = require('../export-vars.js');
const path = require('path');

const PATHS = {
  context: path.resolve(__dirname, "../src/")
};

module.exports = {

  context: PATHS.context,


  plugins: [
    new ev.ExtractTextPlugin({ 
      filename: '[name].css', 
      allChunks: true
    }),

    new ev.GitRevisionPlugin({
      branch: true,
      commithashCommand: 'config user.name'
    }),

    new ev.wp.BannerPlugin({
      banner: "// Build package info" +
      "\nbuildinfo = {" +
      "\n  buildname : \'" + ev.pkg.name + "\'," +
      "\n  ver : \'" + ev.pkg.version + "\'," +
      "\n  template : \'" + ev.pkg.config.template + "\'," +
      "\n};",
      raw: true,
      entryOnly: true,
      exclude: [
        'main.css'
      ]
    }),

    new ev.wp.BannerPlugin({
      banner: ""+ ev.pkg.config.title + " - " + ev.pkg.config.themepath + 
      "\n@link: "+ ev.pkg.config.siteurl +
      "\nSite Template: "+ ev.pkg.config.template +
      "\nBuilt By: "+ ev.pkg.config.name +
      "\nProject Manager: "+ ev.pkg.config.pm +
      "\nDesigner: "+ ev.pkg.config.designer +
      "\n==== Git Info ====" +
      "\nBranch Name: [git-revision-branch]" +
      "\nBuild Version: "+ ev.pkg.version +
      "\nGit Tag: [git-revision-version]" +
      "\nLast built by: [git-revision-hash]"
    }),
  ],

  module: {
    rules: [
      { 
        test: /\.js$/, 
        loader: [
          'imports-loader?define=>false',
          'babel-loader'
        ],
        include: [
          path.resolve(__dirname, '../src')
        ],
        exclude: [
          /(node_modules)/, 
          path.resolve(__dirname, '../src/js/vendor'),
        ]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        //image files
        test:/\.(png|jpg|gif|svg)$/,
        include: [
          path.resolve(__dirname, '../src/images/'),
          path.resolve(__dirname, '../src/components/images/')
        ],
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images/',
              name: '[name].[ext]'
            }
          }
        ]
      },
      {
        //font files
        test:/\.(eot|svg|ttf|woff|woff2)$/,
        include: [
          path.resolve(__dirname, '../src/fonts/'),
          path.resolve(__dirname, '../src/components/fonts/')
        ],
        use: [{
          loader: 'file-loader',
          options: {
            outputPath: 'fonts/',
            name: '[name].[ext]'
          }
        }]
      }
    ],
  },

  resolve: {
    extensions: [".js", ".json", ".jsx", ".css"],
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules')
    ],
    alias: {
      variables: path.resolve(__dirname, '../src/js/config-vars'),
      components: path.resolve(__dirname, '../src/components/')
    }
  },

  externals: {
    jquery: 'jQuery'
  },

  cache: false,

};
