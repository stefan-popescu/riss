// ================================
// ENHANCEMENTS
// ================================
// require('../internal/fs-media-slider');

import {
	backgroundImage
} from '../internal/utilities';

ENHANCEMENTS = {

  init () {
    // this.exampleFunction();
  }, 

  exampleFunction () {
  	backgroundImage('.bg-image');
  },

}; //end ENHANCEMENTS

export default ENHANCEMENTS;