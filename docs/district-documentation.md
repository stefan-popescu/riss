District Documentation
========================

1. set district to true in package.json

```
"district": true,
```

2. open district/district-vars.js and add your schools in the exported district object

```
// district.js object: add schools here
exports.district = {
  schools: [
    "school1",
    "school2",
    "school3"
  ]
};
```
	
3. in terminal cd into your build and execute $ npm run publish

	> first this command will connect you to the server or map your drive, once you are connected run the command again in your terminal this will create all of your sass and js files for you and push them to the server


4. in composer create separate school themes
	- attach only the appropriate school.css files to each theme
	- attach both main.js and respective school.js files to each theme

5. Once you have published your files you can use the same commands for working locally

<br />

**WARNING: compilation time particularly in the production environemnt may take a significant amount of time depending on how many schools are in the district**<br /><br />

## School Sass Partials

**src/district/sass/...**<br />

In order to override !default variables, redefine them above @import "all"; in the individual school partials

```
// src/district/sass/school1.scss

$m1: #000; // school specific $m1
$m2: #ccc; // school specific $m2

etc...

@import "all";
```
<br />

## School Javascript Files

**src/district/js/...**<br />

- /all.js - write javascript that is required on **all** school sites 

	> Each school.js file will by default require("./all.js");


- /school.js - write javascript that is required only on **individual** school site 

<br />

