

// Required Components
// ==========================================================================
const ev = require('../export-vars.js');
const path = require('path');

const merge = require('webpack-merge');
const common = require('../webpack/webpack.common.js');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const dv = require('./district-vars.js');

const webpackDistrictDeploy = merge(common, {

  entry: dv.entryPROD,

  devtool: 'none',

  watch: true,
  watchOptions: {
    ignored: [
      /node_modules/,
      "../src/**/**/**/**",
    ]
  },

  performance: {
    hints: "warning",
    maxEntrypointSize: 500000,
    maxAssetSize: 300000
  },

  plugins: [

    new ev.UglifyJsPlugin(),

    new ev.wp.optimize.CommonsChunkPlugin({
      children: true,
    }),

    new BrowserSyncPlugin({
      host: 'localhost',
      port: 5000,
      proxy: ev.pkg.config.siteurl + '/' + ev.pkg.config.startpage,
      notify: false,
      // files: [ 
      //   ev.directory + 'main.css', 
      // ],
    }, {
        reload: true,
    }),
  ],

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ev.ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader'},
            { loader: 'postcss-loader'},
            { loader: 'resolve-url-loader'},
            { loader: 'sass-loader',
              options: {
                sourceMap: true,
                outputStyle: 'compact',
                data: '@import "include";',
                includePaths: [
                  path.resolve(__dirname, '../src/sass/')
                ]
              }
            }
          ]
        })
      }
    ],
  },

  output: {
    path: ev.directory,
    filename: '[name].js'
  }

});

module.exports = webpackDistrictDeploy;