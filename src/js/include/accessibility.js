// ================================
// ADA
// ================================

require('../internal/menu-accessibility');

import {
    notDraftMode,
    $navMain_level1,
    $siteSearch,
    $section
} from './config-vars';

ACCESSIBILITY = {

  init () {
    if (notDraftMode) {
      this.accessibleNavigation();
    }

    this.accessibleSearch();
    // this.googleTranslateAccessibility();
    // this.accessibleSectionLabel();
  },


  // Accessible Main Navigation
  // allows user to tab through main navigation and dropdown links
  // ==============================================================

  accessibleNavigation () {
    $navMain_level1.accessibility_menu();
  },


  // Custom Accessible Search
  // allows user to tab to open a collapsed custom search 
  // =====================================================

  // <div class="top-search">
  //   <form method="get" action="/search-results" class="search-form" role="search">
  //     <label for="search-text">Search:</label>
  //     <input type="text" name="q" aria-label="search text" id="search-text" placeholder="Keyword">
  //     <button type="submit">Search</button>
  //   </form>
  // </div>

  accessibleSearch () {

    $siteSearch.on('focusin click', function() {
        $(this).addClass("search-open");
    });

    $siteSearch.on('focusout', function() {
        $(this).removeClass("search-open");
    });

    $(document).on('click', function(e) {
        if (!$(e.target).closest($siteSearch).length) {
          $siteSearch.removeClass("search-open");
        }
    });
  },


  // Section Aria Label
  // find element title and attributes it as the aria-label
  // NOTE: this is a product fix and checks if the label already exists
  // ===================================================================

  accessibleSectionLabel () {

      $section.each(function(){
          var sectionTitle = $(this).find('> header .fsElementTitle').text();
          if ( !$(this).attr('aria-label') ){
              $(this).attr('aria-label', sectionTitle);
          }
      });
  },


  // Google Translate Accessibility 
  // adds aria-label to the google translate form 
  // NOTE: this is a third party fix and checks if it already exists
  // ================================================================

  googleTranslateAccessibility () {

      if($("#google_translate_element").length) {
          var maxTranslateAttempts = 50;
          var checkTranslate = setInterval(function() {
              maxTranslateAttempts -= 1;
              if($("#google_translate_element select.goog-te-combo").length) {
                  clearInterval(checkTranslate);
                  if((!$("#google_translate_element select.goog-te-combo").parent()[0].length && $("#google_translate_element select.goog-te-combo").parent()[0].tagName !== "LABEL") && (!$("#google_translate_element select.goog-te-combo").prev().length || $("#google_translate_element select.goog-te-combo").prev()[0].tagName !== "LABEL")) {
                      $("#google_translate_element select.goog-te-combo").before('<label for="select-translate">Translate Website</label>');
                      $("#google_translate_element select.goog-te-combo").attr("id","select-translate");
                  }
              } else if(maxTranslateAttempts === 0) {
                  clearInterval(checkTranslate);
              }
          }, 200);
      }
  }

}; //end ACCESSIBILITY

export default ACCESSIBILITY;
