#Code review checklist#

## Code Management ##
- Project has a branch in Stash and it is up to date
- Large photos aren't checked into the branch


## CSS/Sass ##
- Selectors in scss files are not nested deeper than 3
- Naming conventions for classes make sense and are consistent

## Software ##
- Proper use of Composer elements
- Compose mode works, content is client editable

## Javascsript ##
- Don't replace element ID's, they are used by Composer
	- Elements that are cloned should have their IDs removed, or changed
- enquire is used properly

## Design ##
- Style guide and production pages are complete and accurate
	- try to reproduce items that are on the production page 
	- use branch for the quicklinks
	- cta buttons are reproducable
	- all style manager entries should appear in dropdowns
	- table headers
	- element titles are styled the same across all element types
	- content elements vs news lists used for sliders
- Site notes page is in place
	- create instructions for clients/pm to use for recreating anything
