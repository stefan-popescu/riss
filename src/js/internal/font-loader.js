// https://github.com/typekit/webfontloader

//typical vendor examples to add to global.js
/*

  google: {
    families: ['Oxygen', 'Playfair Display']
  }

  monotype: { //fonts.com
    projectId: '721fb5b4-1b7f-4469-ad10-9139a9829756'
  }

  typekit: {
    id: 'ycj7wse'
  }


  Note: only include the vendor definitions that you need in WebFontConfig
  Including empty attributes will cause a load error
*/


(function(d) {

  function insertScript(d){
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
  }
  insertScript(d);

  //check for form iframes and insert webfont script
  var forms = d.getElementsByClassName('fsForm'), f;
  if ( forms.length ){
    for( var i=0; i < forms.length; i++ ){
      f = forms[i].getElementsByTagName('iframe')[0];
      f.onload = function(){
        this.contentWindow.WebFontConfig = window.WebFontConfig;
        insertScript(this.contentDocument);
      }
    } 
  }

})(document);
