

// Required Components
// ==========================================================================
const ev = require('../export-vars.js');
const path = require('path');

const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';

const PATHS = {
  context: path.resolve(__dirname, "../src/"),
  form: path.join(__dirname, "../src/js/form"),
  main: path.join(__dirname, "../src/build.js"),
};

const webpackconfig = merge(common, {

  entry: {
    main: [PATHS.main, hotMiddlewareScript],
    form: [PATHS.form, hotMiddlewareScript]
  },

  devtool: 'inline-cheap-module-source-map',

  plugins: [

    new ev.wp.HotModuleReplacementPlugin()

  ],

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ev.ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: {sourceMap: true}},
            { loader: 'postcss-loader', options: {sourceMap: true}},
            { loader: 'resolve-url-loader'},
            { loader: 'sass-loader',
              options: {
                data: '@import "include";',
                sourceMap: true,
                includePaths: [
                  path.resolve(__dirname, "../src/sass/")
                ]
              }
            }
          ]
        })
      }
    ],
  },

  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].js',
    publicPath: '/uploaded/themes/' + ev.pkg.config.themepath
  }

});

module.exports = webpackconfig;