// // ====================================================
// // Custom Finalsite Feeds Plugin
// // ====================================================

// this function is used in conjunction finalsite feeds and draws from the API
// example API URL json output: http://feeds.finalsite.com/api/feeds/lovett-base

// in addition: to pull in one feed per element (ie grid/mashup yada yada)
// snippet: https://stash.fs4.us/snippets/2c06fc268ccb459d94a3a1fd4d4c3d2e
// example page: http://detroitjesuit.finalsite.com/

// // use case: 
// $('.custom-feed').each(function() {
//   var self = $(this);

//   self.fsFeedPull(5, {
//     feedTemplate:[
//       '<article class="fsFeed-post">',
//         '<img src="{imgSrc}" class="feed-img" />',
//         '<div class="feed-content">',
//           '<div class="like-count">{likeCount}</div>',
//           '<div class="feed-desc">{feedDesc}</div>',
//         '</div>',
//       '</article>'
//     ], // html markup

//     callback: function() {

//       //write all functions to be run after media data has been pulled in here
//       // ie (slick, randomize, etc...)

//     }
//   });
// });

import {
  nano
} from './utilities';

$.fn.fsFeedPull = function( postNumber, options ) {

  targetClass = this;

  var $fsFeedContainer = $(targetClass),
      $feedURL = $fsFeedContainer.attr('data-feed-url'),

      $feedData = {
        per : postNumber, // The number of posts to show on a page of reponses
        page : 1, // The page of posts to return in the response 
        // filter : null, // Pass in either a name of a social account (capitalized) like "Facebook" or "LinkedIn" to only show posts from that source
        // starting_at : null, // Pass in a date in the format YYYY-MM-DD HH:MM (HH:MM optional) to show posts newer than this date
        // ending_at : null // Pass in a date in the format YYYY-MM-DD HH:MM (HH:MM optional) to show posts older than this date
      }
  ;

  var settings = $.extend({
      feedTemplate: ''
  }, options );


  var postTemplate = {
    'slide' : settings.feedTemplate.join('\n')
  };

  $.getJSON($feedURL, $feedData, function(data) {
      $.each(data.posts.items, function(i,object){

        $fsFeedContainer.find(">.fsElementContent").append(
            nano(postTemplate.slide, {
              'imgSrc': data.posts.items[i].image,
              'likeCount': data.posts.items[i].like_count,
              'feedDesc': data.posts.items[i].unformatted_message,
              'itemURL': data.posts.items[i].full_url,
              'feedType': data.posts.items[i].source.source
        }));

    });

  }).done(function() {
    
    options.callback();

  }).fail(function() {

      $fsFeedContainer.append('<span>Sorry, an error occured when retrieving this feed data. Please refresh the page to try again.</span>').css('textAlign', 'center');

  });
};
